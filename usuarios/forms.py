from django import forms
from usuarios.models import Pessoa

class PessoaForm(forms.ModelForm):
    senha = forms.CharField(widget=forms.PasswordInput, required=True)
    matricula = forms.IntegerField(required=True)
    class Meta:
        model = Pessoa
        fields = ['matricula', 'senha']

    # def is_valid(self):


