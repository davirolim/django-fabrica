from django.db import models


class Pessoa(models.Model):
    name = models.CharField(max_length=100)
    cpf = models.CharField(max_length=100)
    matricula = models.CharField(max_length=100)
    senha = models.CharField(max_length=25)

    def __str__(self):
        return self.name



class Data(models.Model):
    pessoa = models.ForeignKey(Pessoa, related_name='ponto', on_delete=models.CASCADE)
    bateuPonto = models.CharField(max_length=30)

    def __str__(self):
        return str(self.pessoa) + ' - ' + self.bateuPonto

