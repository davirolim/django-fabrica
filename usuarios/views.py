from django.shortcuts import render, HttpResponseRedirect
from django.views import generic
from django.views.generic import View
from .models import Pessoa
from usuarios.forms import PessoaForm
from datetime import datetime
from .models import Pessoa, Data
from django.shortcuts import HttpResponse


def baterPonto(request):
    # now = datetime.datetime.now()
    return render(request, 'usuarios/baterponto.html')

def pessoaView(request):
      if request.method == "POST":
          form = PessoaForm(request.POST)
          if form.is_valid():
             post = form.save(commit=False)
             try:
                 if Pessoa.objects.get(matricula=post.matricula):
                     p1 = Pessoa.objects.get(matricula=post.matricula)
                     if post.senha == p1.senha:
                         successful_login = "Registrado com sucesso"
                         agora = datetime.now()
                         agora = agora.strftime('%d/%m/%Y %H:%M:%S')
                         Data.objects.create(pessoa=p1, bateuPonto=agora)
                         return render(request, 'usuarios/baterponto.html', {'form':form, 'sucesso_message':successful_login})

             except:
                 error_message = "Dados incorretos"
                 return render(request, 'usuarios/baterponto.html', {'form':form, 'error':error_message})

             error_message = "Dados incorretos"
             return render(request, 'usuarios/baterponto.html', {'form':form, 'error':error_message})
      else:
          form = PessoaForm(None)
      return render(request, 'usuarios/baterponto.html', {'form':form})

